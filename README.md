# BoogeyBots FTC Team Code

This repository hosts the code for the robots of the BoogeyBots FTC team, based in Ramnicu Sarat, Romania.

THE CODE HERE IS NOT COMPLETELY FUNCTIONAL AS I MANAGED TO FORGET TO PUSH THE LATEST COMMITS AND THE I WIPED THE HARD DRIVE THAT CONTAINED THE CODE BECAUSE OF MY SEVERE CONDITION CALLED DISTRO-HOPPING.

## Documentation

The programming part of the engineering notebook is hosted at [brown121407/ftc-programming-notebook](https://github.com/brown121407/ftc-programming-notebook).

## Technologies used

- [Android Studio](https://developer.android.com/studio/)
- [Kotlin](https://kotlinlang.org/)
- [Tensorflow Lite](https://www.tensorflow.org/lite/)
